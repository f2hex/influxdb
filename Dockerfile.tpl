FROM f2hex/arm64-alpine:3.8

MAINTAINER Franco Fiorese <franco.fiorese@gmail.com>

# WARNING: at the moment the binary tarball of the project is not yet
# architecture idendependent

# Use a specific vesion of InfluxDB
ARG INFLUXDB_VERSION 1.7.2

# the location where to find a tarball package with InfluxDB binaries
# to be installed
ENV INFLUXDB_BINPLOC "https://dl.influxdata.com/influxdb/releases"

# set precedence of net name resolution when container is running
RUN echo 'hosts: files dns' >> /etc/nsswitch.conf

# ensure a proper availability of CA certificates (Mozilla Certificate Store)
RUN    apk add --no-cache tzdata bash ca-certificates \
    && update-ca-certificates

# trap any error as an exit condition
RUN    set -e \
    && apk add --no-cache --virtual .build-deps wget gnupg tar \
    && for key in \
            05CE15085FC09D18E99EFB22684A14CF2582E0C5 ; \
       do \
           gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key" || \
           gpg --keyserver pgp.mit.edu --recv-keys "$key" || \
           gpg --keyserver keyserver.pgp.com --recv-keys "$key" ; \
       done \
    && cd /root \
    && wget --no-verbose ${INFLUXDB_BINPLOC}/influxdb-${INFLUXDB_VERSION}_linux_arm64.tar.gz.asc \
    && wget --no-verbose ${INFLUXDB_BINPLOC}/influxdb-${INFLUXDB_VERSION}_linux_arm64.tar.gz \
    && gpg --batch --verify influxdb-${INFLUXDB_VERSION}_linux_arm64.tar.gz.asc influxdb-${INFLUXDB_VERSION}_linux_arm64.tar.gz \
    && tar -xz -f influxdb-${INFLUXDB_VERSION}_linux_arm64.tar.gz \
    && chmod +x influxdb-*/usr/bin/* \
    && cp -a influxdb-*/usr/bin/* /usr/bin/ \
    && cp -a influxdb-*/usr/lib/* /usr/lib/ \
    && cp -a influxdb-*/usr/share/* /usr/share/ \
    && rm -rf influxdb-* .gnupg \
    && apk del .build-deps

COPY influxdb.conf /etc/influxdb/influxdb.conf
COPY entrypoint.sh /entrypoint.sh
COPY init-influxdb.sh /init-influxdb.sh
RUN  chmod +x  /entrypoint.sh /init-influxdb.sh

EXPOSE 8086

VOLUME /var/lib/influxdb

ENTRYPOINT ["/entrypoint.sh"]
CMD ["influxd"]
